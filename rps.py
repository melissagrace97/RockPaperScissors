import random
choices = {'rock': 0, 'paper': 1, 'scissor': 2} #declaring variables
player_wins = computer_wins = draw_count = 0
history_of_choices = {}
def results(): #function to display the points obtained
    print("POINTS OBTAINED:\n"+ "Player : {}\n".format(player_wins)+"Computer : {}".format(computer_wins))
def game(player, computer): #function to check the results
    global player_wins, computer_wins, draw_count
    win = {0:2, 2:1, 1:0}
    if player == computer: #to check if the player and computer results in a draw
        draw_count += 1
        result = 'TIE'
    elif win[player] == computer:
        player_wins += 1
        result = 'Player Wins'
    else:
        computer_wins += 1
        result = 'Computer Wins'
    return result
print("     ROCK PAPER SCISSORS GAME\n\n")
for round in range(1,11):#repeat for 10 rounds
    computer = random.choice(['rock','paper','scissor'])#to store computer choices
    print("     ROUND {}\n".format(round))
    player = input("What is your choice?\n rock\n paper \n scissor \n Enter the choice:")#to store player choices
    player_choice = choices[player]
    computer_choice = choices[computer]
    print("Player choice: {}\n".format(player)+"Computer choice: {}\n".format(computer))#to display the choices made by player and computer
    result = game(player_choice, computer_choice)
    history_of_choices[str(round)] = [computer, player, result]
results()
if player_wins > computer_wins:
    print("PLAYER IS THE WINNER OF THE GAME!\n\n")
elif computer_wins > player_wins:
    print("COMPUTER IS THE WINNER OF THE GAME!\n\n")
else:
    print("The game has ended in a draw!\n\n")
while True:
    ch = int(input("Enter the round for which you need the information:"))
    print("Game Result: {}\n".format(history_of_choices[str(ch)][2])+"Player choice: {}\n".format(history_of_choices[str(ch)][1])+"Computer choice: {}\n\n".format(history_of_choices[str(ch)][0]))
    check_again = input('Would you like to check the choice made for another round [y/n]? ')
    if check_again.lower() == 'n':
        break